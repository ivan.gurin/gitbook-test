# Summary

* [Introduction](README.md)
	* [Convention on convention]()
	* [Amending this convention]()
* [Architecture](architecture/README.md)

* [Coding style]()
	* [C#](coding-style/c-sharp/README.md)
		* [EPP10001](coding-style/c-sharp/EPP10001.md)
	* [Angular/TS]()
	* [JS]()
* [UI/UX]()
	* [Design guidelines]()
	* [URLs]()
	* [Form fields]()
	* [Buttons]()
	* [Error messages]()
* [Processes]()
	* [Jira]()
	* [Gitlab]()


