You've got into the most nerdy place of EPP - Coding Conventions. Proceed on your own risk.

## The purpose of this document

Readability, fast entry, easy reviewing 

## Terminology

MUST, SHOULD, MAY

## Useful links

Confluence stuff

## Credits

This document was inspired by [Aviva Solutions C# Coding Guidelines](https://www.csharpcodingguidelines.com/) [GitHub](https://github.com/dennisdoomen/CSharpGuidelines)  
